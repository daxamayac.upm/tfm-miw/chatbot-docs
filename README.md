# Trabajo de Fin de Máster - Máster en Ingeniería Web UPM

# [Chatbot - VixLum](https://vixlum.com)

> En este proyecto se desarrolla un bot conversacional para mejorar la interacción con los clientes de la microempresa VixLum.  
El front-end es un pequeño widget para incrustar en el sitio web de vixlum y el back-end es el bot construido con Rasa OpenSource, que accederá a una pequeña base de datos para proporcionar precios de varios productos.  
> La conversación puede ser transferida del bot hacia un asesor por medio de un canal de Slack, bajo petición del cliente.

## Tecnologías necesarias

`Rasa Open Source` `Python` `React` `JavaScript` `GitLab` `GitLab CI/CD` `SonarCloud` `Slack` `MongoDB` `Docker` `Heroku` `ngrok` `plantuml`

## Gestión

La gestión se encuentra en el grupo creado en GitLab  
[Grupo-GitLab](https://gitlab.com/daxamayac.upm/tfm-miw)

- [Board](https://gitlab.com/groups/daxamayac.upm/tfm-miw/-/boards)
- [Historias de Usuario](/src/stories)

## Estado del código

Proyecto | GitLab CI/CD | SonarCloud  
-- | -- | --
[Chatbot-widget](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-widget) | [![pipeline status](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-widget/badges/develop/pipeline.svg)](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-widget/-/commits/develop) | [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=daxamayac.upm_chatbot-widget&metric=alert_status)](https://sonarcloud.io/dashboard?id=daxamayac.upm_chatbot-widget)
[Chatbot-rasa](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-rasa) | [![pipeline status](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-rasa/badges/develop/pipeline.svg)](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-rasa/-/commits/develop) | [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=daxamayac.upm_chatbot-rasa&metric=alert_status)](https://sonarcloud.io/dashboard?id=daxamayac.upm_chatbot-rasa)

## Despliegue de prueba en Heroku

Como es un despliegue de prueba gratuito, Heroku suspende los despliegues después de un tiempo de inactividad. Sé
paciente puede tomar un momento en funcionar :).

### [Prueba el Widget](https://chatbot-widget.herokuapp.com) Está listo cuando se puede ver el widget.

### [Rasa bot](https://chatbot-vixlum-rasa.herokuapp.com) Está listo cuando muestra la versión de Rasa.
### [Rasa action server](https://chatbot-vixlum-actions.herokuapp.com) Está listo cuando muestra "Requested URL / not found".

`Cuando los proyectos esten listos, puedes empezar una conversación a través del widget.`

## :gear: Instalación del proyecto

### chatbot-widget

Clonar repositorios, **mediante consola**:

```sh
> cd <folder path>
> git clone https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-widget.git
> cd chatbot-widget
>chatbot-widget> yarn install
```

`No olvidar cambiar el host de index.html, con la dirección del bot
"host: "http:<host>/webhooks/rest/webhook"
`

Desarrollo continuo, **mediante consola** (en dos terminales diferentes):

```sh
>chatbot-widget> yarn watch
```

```sh
>chatbot-widget> yarn serve
```
Para revisar el código el proyecto puede ser importando en IntelliJ IDEA  
Open `http://localhost:8080/index.html` in your browser.

Construir para producción

```sh
>chatbot-widget> yarn build
```

### chatbot-rasa


Clonar repositorios, **mediante consola**:

```sh
> cd <folder path>
> git clone https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-rasa.git
```
Importar con pyCharm y crear un entorno de python virtual con Python 3.8.10


```sh
> pip install requirements-dev.txt
> pip3 install rasa
```
`para probar la capacidad de traspaso a un canal de slacks e debe configurar la aplicacion de slack y obtener los token de bot y de aplicacion`

No olvides entrenar a tu bot para tener un modelo actualizado
```sh
>cd src
>rasa train
```
Para ejecutar se necesita iniciar el servidor de acciones personalizadas y el bot rasa en diferentes terminales
```sh
>cd src
>rasa run actions
```
Ejecucion solo por consola
```sh
>cd src
>rasa shell
```
Construir docker de pendiendo del entorno del depliegue 

```sh
>docker build . -f ./docker/vixlum/Dockerfile.actions -t $REGISTRY_USERNAME/vixi-actions:latest
```

# Documentación

[Chatbot-docs](https://gitlab.com/daxamayac.upm/tfm-miw/chatbot-docs)

### Funcionalidad general

Se tiene un bot que responderá los mensajes de los clientes, si el cliente solicita un asesor humano el bot se encarga
de transferirlo si hay uno disponible.

![Bot](src/diagrams/svg/functionality.svg)

### Funcionalidad

Diagrama de secuencia cuando los mensajes son respondidos por el bot y cuando el cliente solicita un asesor humano
![Bot](src/diagrams/svg/conversationWithBot.svg)
![Bot](src/diagrams/svg/clientRequestAdvisor.svg)
![Bot](src/diagrams/svg/conversationWhitAdvisor.svg)


### Mockup Widget

![Bot](src/diagrams/svg/widgetMockup.svg)

### Base de datos

![Bot](src/diagrams/svg/mongoOrganizationSchema.svg)
![Bot](src/diagrams/svg/mongoProductSchema.svg)

# Despliegue

## Pruebas

Se depliega en Heroku para realizar pruebas
![Bot](src/diagrams/svg/deployTest.svg)

## Producción
![Bot](src/diagrams/svg/deployProduction.svg)
