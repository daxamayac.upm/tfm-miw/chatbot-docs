# SCRUM

Para la gestión de este proyecto se adapta SCRUM.  
Se tiene las Historias de Usuario donde el producto final agrega valor al negocio y las tareas para cumplir cada
Historia de Usuario se las representará como issues en la pizarra de GitLab

[comment]: <> (#######)

## Historias Técnicas

> ### Historia Técnica 01: Preparation GitLab group
>Preparar el grupo de Gitlab y la los sub-repositorios  
> <span style="font-size:1.3em">**Como:**</span>
> Desarrollador  
> <span style="font-size:1.3em">**Quiero:**</span>
> Tener un organizados los proyectos en un grupo de GitLab  
> <span style="font-size:1.3em">**Para poder:**</span>
> Poder realizar una correcta gestión y organización del proyecto
>### Tareas
> - Crear Grupo en GitLab
> - Crear Repositorios docs, widget y rasa
    > -Crear labels, milestones, issues y organizar.
> ### Criterios Aceptación

[comment]: <> (#######)

> ### Historia Técnica 02: Preparation chatbot-docs
> Plasmar la documentación más importante  
> <span style="font-size:1.3em">**Como:**</span>  
> <span style="font-size:1.3em">**Quiero:**</span>  
> <span style="font-size:1.3em">**Para poder:**</span>  
>### Tareas
> - chatbot-docs (estructura de directorios)
> - chatbot-docs (Escribir las historias de usuario)
> - chatbot-docs (pasar los diagramas a plantuml, y redactar los readme)

[comment]: <> (#######)

> ### Historia Técnica 03: Preparation chatbot-widget
> Preparar el esqueleto del proyecto  
> <span style="font-size:1.3em">**Como:**</span>  
> <span style="font-size:1.3em">**Quiero:**</span>  
> <span style="font-size:1.3em">**Para poder:**</span>  
>### Tareas
> - chatbot-widget: skeleton project from https://github.com/RasaHQ/chatroom
> - chatbot-docs: Escribir las historias de usuario)
> - chatbot-docs: pasar los diagramas a plantuml, y redactar los readme)

[comment]: <> (#######)

> ### Historia Técnica 04: Preparation chatbot-rasa
> Preparar el esqueleto del proyecto  
> <span style="font-size:1.3em">**Como:**</span>  
> <span style="font-size:1.3em">**Quiero:**</span>  
> <span style="font-size:1.3em">**Para poder:**</span>  
>### Tareas
> - chatbot-rasa: skeleton project
> - chatbot-docs:Escribir las historias de usuario
> - chatbot-docs: pasar los diagramas a plantuml, y redactar los readme

[comment]: <> (#######)

[comment]: <> (#######)

## Historias de Usuario

> ### Historia Usuario 01: Basic chatbot
> <span style="font-size:1.3em">**Como:**</span> Dueño de VixLum - Adriana Montalvo  
> <span style="font-size:1.3em">**Quiero:**</span> Un chat automático en mi página web  
> <span style="font-size:1.3em">**Para poder:**</span> Para que atendida de forma rápida a mis clientes
> ### Tareas
> - Construir el widget
> - Construir el bot
> - Conectar widget y bot
>
> ### Criterios Aceptación
> El bot responderá saludos, despedidas, solicitud del nombre en idioma español

[comment]: <> (#######)

> ### Historia Usuario 02: Bot answers faq
> <span style="font-size:1.3em">**Como:**</span> Dueño de VixLum - Adriana Montalvo  
> <span style="font-size:1.3em">**Quiero:**</span> El chat responda automáticamente las preguntas frecuentes.  
> <span style="font-size:1.3em">**Para poder:**</span> Para que atendida de forma rápida a mis clientes
> y los empleados inviertan mejor su tiempo
> ### Tareas
> - Las respuestas de las FAQ son respondidas desde una base de datos
> - Se implementan acciones personalizadas en Rasa
>
>### Criterios Aceptación
> El bot responderá las FAQ como
> - ¿Dónde están ubicados?
> - ¿En qué horario atienden?
> - ¿Cuál es su correo?
> - ¿Qué servicios ofrecen?
> - ¿Qué es VixLum? ¿Qué hace su empresa?
>
> #### Respuesta por defecto
> - No he entendido la pregunta, lo podrías repetir de otra manera....

[comment]: <> (#######)

> ### Historia Usuario 03: Bot human handoff
> <span style="font-size:1.3em">**Como:**</span>
> Dueño de VixLum - Adriana Montalvo    
> <span style="font-size:1.3em">**Quiero**</span>
> Tener la capacidad de reemplazar al bot en una conversación
> bajo petición del usuario  
> <span style="font-size:1.3em">**Para poder:**</span>
> Responder las preguntas que el bot no pueda
>
>### Tareas
> - Bot verifica que hay un asesor conectado
> - El bot crea un canal invitando al asesor
> - Se envía el historial de conversación
> - Se realiza el traspaso de canal de bot a slack
>
> ### Criterios Aceptación
> El bot realiza el traspaso a un asesor humano con solicitudes similares a:
> - Human??
> - Quiero un asesor
> - ¿Me puede ayudar alguien?
> - ¿Quisiera hablar con alguien más?
> #### Respuesta por defecto (asesor no disponible)
> - En este momento no hay asesores disponibles, deseas esperar

[comment]: <> (#######)

> ### Historia Usuario 04: Bot provides product prices
> El bot puede traspasar la conversación a un asesor humano, bajo pedido del cliente
>### Tareas
> - Conectar consultas de productos a base de datos
> - El bot puede identificar preguntas de precios de un producto.
> - El bot responde el precio de un producto consultado en la base de datos.
>
> ### Criterios Aceptación
> El bot responderá preguntas de tipo consulta de precio de productos
> - ¿Cuánto vale un <<product>>?
> - ¿Cuánto cuesta un <<product>>?
> - ¿Cual es el precio de un <<product>>?
> - ¿Quiero saber el precio de un <<product>>?
